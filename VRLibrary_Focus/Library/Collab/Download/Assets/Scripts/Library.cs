﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Library : MonoBehaviour
{

    [Range(1, 50)]
    public int nbColumn = 2;
    [Range(1, 50)]
    public int nbRow = 4;
    [Range(1, 50)]
    public int sizeRow = 3;
    [Range(2, 8)]
    public int bookShelfHeight = 5;
    [Range(1, 14)]
    public int booksPerShelf = 10;
    [Range(0.01f, 1f)]
    public float clippingPeriod = 0.2f;
    public float radiusFOV = 1;
    public float lenghtFOV = 5;

    public GameObject headset;

    private GameObject library;
    private const int labelMax = 17575;
    private const int shelfWidth = 7;
    private const int columnSpace = 10;
    private float lastTime;
    public const int rowSpace = 12;

    public struct Position
    {
        public float posX;
        public float posY;
        public float posZ;

        public Position(float x, float y, float z)
        {
            posX = x;
            posY = y;
            posZ = z;
        }
    }

    void Start()
    {
        Physics.gravity = new Vector3(0, -60.0F, 0);
        PopulateLibrary();
        lastTime = 0;
        //Debug.Log("Row: " + Row.rowCount + "; BookShelf: " + BookShelf.bookShelfCount + "; Shelf: " + Shelf.shelfCount + "; Book: " + Book.bookCount);
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(1))
        {
            DestroyLibrary();
            PopulateLibrary();
            //Debug.Log("Row: " + Row.rowCount + "; BookShelf: " + BookShelf.bookShelfCount + "; Shelf: " + Shelf.shelfCount + "; Book: " + Book.bookCount);
        }
        if (Time.fixedTime - lastTime > clippingPeriod)
        {
            BookClipping();
            ShelfClipping();
            lastTime = Time.fixedTime;
        }
    }

    public void PopulateLibrary()
    {
        
        
        library = GameObject.CreatePrimitive(PrimitiveType.Cube); // Instantiate floor
        library.transform.position = new Vector3((sizeRow * shelfWidth * nbColumn + (nbColumn - 1) * columnSpace) / 2, -0.25f / 2, nbRow * rowSpace / 2 - 10);
        library.transform.localScale = new Vector3(sizeRow * shelfWidth * nbColumn + nbColumn * columnSpace + 10, 0.1f, nbRow * rowSpace);
        library.name = "Library";
        
        
        GameObject floor = Instantiate(GameObject.CreatePrimitive(PrimitiveType.Cube));
        floor.transform.position = new Vector3((sizeRow * shelfWidth * nbColumn + (nbColumn - 1) * columnSpace) / 2, -0.25f / 2, nbRow * rowSpace / 2 - 10);
        floor.transform.localScale = new Vector3(sizeRow * shelfWidth * nbColumn + nbColumn * columnSpace + 10, 0.26f, nbRow * rowSpace);
        floor.name = "Floor";
        floor.tag = "AllowTeleport";
        
        floor.transform.parent = library.transform;

        for (int i = 0; i < nbColumn - 1; i++)
        {
            new Column(i, nbRow, new Label(i * labelMax / nbColumn), new Label(((i + 1) * labelMax / nbColumn) - 1), new Position(i * (sizeRow * shelfWidth + columnSpace), 0, 0), library);
        }
        new Column(nbColumn - 1, nbRow, new Label((nbColumn - 1) * labelMax / nbColumn), new Label(labelMax), new Position((nbColumn - 1) * (sizeRow * shelfWidth + columnSpace), 0, 0), library);
    }

    public void DestroyLibrary()
    {
        Destroy(library);
        Row.rowCount = 0;
        BookShelf.bookShelfCount = 0;
        Shelf.shelfCount = 0;
        Book.bookCount = 0;
    }

    private void BookClipping()
    {
        for (int i = 0; i < Book.bookCount; i++)
        {
            GameObject book = Book.list[i].GetGameObject();
            if (!IsInFOV(book.transform.position))
            {
                if (book.GetComponentInChildren<Canvas>().enabled)
                    book.GetComponentInChildren<Canvas>().enabled = false;
            }
            else
            {
                if (!book.GetComponentInChildren<Canvas>().enabled)
                    book.GetComponentInChildren<Canvas>().enabled = true;
            }
        }
    }

    private void ShelfClipping()
    {
        GameObject shelf;
        for (int i = 0; i < BookShelf.bookShelfCount; i++)
        {
            for (int j = 1; j < bookShelfHeight * 2; j += 2)
            {
                shelf = BookShelf.list[i].GetGameObject().transform.GetChild(j).gameObject;

                if (!IsInFOV(BookShelf.list[i].GetGameObject().transform.position))
                {
                    if (shelf.GetComponentInChildren<Canvas>().enabled)
                        shelf.GetComponentInChildren<Canvas>().enabled = false;
                }
                else
                {
                    if (!shelf.GetComponentInChildren<Canvas>().enabled)
                        shelf.GetComponentInChildren<Canvas>().enabled = true;
                }
            }
        }
    }

    private bool IsInFOV(Vector3 bookPosition)
    {
        bool ret = false;
        Vector3 bookRelative = headset.transform.InverseTransformPoint(bookPosition);
        if ((Mathf.Pow(bookRelative.x, 2) + Mathf.Pow(bookRelative.y, 2)) < Mathf.Pow(radiusFOV, 2) && bookRelative.z < Mathf.Sqrt(Mathf.Pow(radiusFOV, 2) - Mathf.Pow(bookRelative.x, 2)) + lenghtFOV && bookRelative.z < Mathf.Sqrt(Mathf.Pow(radiusFOV, 2) - Mathf.Pow(bookRelative.y, 2)) + lenghtFOV && bookRelative.z > (lenghtFOV / radiusFOV) * Mathf.Abs(bookRelative.x) && bookRelative.z > (lenghtFOV / radiusFOV) * Mathf.Abs(bookRelative.y))
                ret = true;
        return ret;
        //if (Vector3.Distance(headset.transform.position, bookPosition) < 15)
    }

    public Vector3 PositionToVector3(Position pos)
    {
        return new Vector3(pos.posX, pos.posY, pos.posZ);
    }
}

public class Label
{

    public readonly string name;
    public readonly int decimalID;

    public Label(int decimalLabel)
    {
        decimalID = decimalLabel;
        name = DecimalToString(decimalID);
    }

    public string DecimalToString(int decimalLabel)
    {
        char[] toChar = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' };
        int label1 = (decimalLabel % (int)Mathf.Pow(26, 2)) % 26;
        int label2 = ((decimalLabel % (int)Mathf.Pow(26, 2)) - label1) / 26;
        int label3 = (decimalLabel - (decimalLabel % (int)Mathf.Pow(26, 2))) / (int)Mathf.Pow(26, 2);
        return "" + toChar[label3] + toChar[label2] + toChar[label1];
    }

    public override string ToString()
    {
        return "Label: " + name + " " + decimalID;
    }

}
