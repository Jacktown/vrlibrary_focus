﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Book {

    private GameObject book;
    private Label cover;
    private Size size;
    private Library.Position position;

    public static int bookCount = 0;
    public static List<Book> list = new List<Book>();

    public struct Size
    {
        public float width;
        public float height;
        public float depth;

        public Size(float x, float y, float z)
        {
            width = x;
            height = y;
            depth = z;
        }
    }

    public Book(Label label, Size _size, Library.Position _position, GameObject shelf)
    {
        cover = label;
        size = _size;
        position = _position;
        bookCount++;
        list.Add(this);
        InstantiateBook(shelf);
        DebugPrint();
    }

    private void InstantiateBook(GameObject parent)
    {
        book = Object.Instantiate(GameObject.Find("BookModel"));
        book.transform.position = new Vector3(position.posX, position.posY, position.posZ);
        book.transform.localScale = new Vector3(size.depth * 0.8f, size.height, size.width);
        book.name = "Book " + cover.name;
        Color newColor = Random.ColorHSV(0f, 1f, 0.5f, 1f, 0.2f, 0.8f);
        book.transform.GetComponent<Renderer>().material.color = newColor;
        float hue, saturation, value;
        Color.RGBToHSV(newColor, out hue, out saturation, out value);
        Text[] newText = book.GetComponentsInChildren<Text>();
        for (int i = 0; i< 3; i++)
        {
            newText[i].text = cover.name[i].ToString();
            newText[i].transform.localScale = new Vector3(1, book.transform.localScale.z, 1);
            if ((value < 0.6f && saturation > 0.6f) || value < 0.5f || ((hue > 0.6f || hue < 0.1f) && saturation > 0.5f))
                newText[i].color = Color.white;
        }
        book.transform.SetParent(parent.transform);
    }

    public GameObject GetGameObject()
    {
        return book;
    }

    private void DebugPrint()
    {
        //Debug.Log("Book: " + label + " position: " + position.posX + ";" + position.posY + ";" + position.posZ + "\n");
        //Debug.Log("Width: " + size.width + " Height: " + size.height + " Depth: " + size.depth);
    }
}
