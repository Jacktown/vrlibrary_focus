﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using wvr;
using WaveVR_Log;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UserControllerListener : MonoBehaviour
{
    private static string LOG_TAG = "UserControllerListener";
    public WVR_DeviceType device = WVR_DeviceType.WVR_DeviceType_HMD;
    
    private bool _triggerState = false;
        
    WVR_InputId[] buttonIds = new WVR_InputId[] {
	    WVR_InputId.WVR_InputId_Alias1_Menu,
	    WVR_InputId.WVR_InputId_Alias1_Grip,
	    WVR_InputId.WVR_InputId_Alias1_DPad_Left,
	    WVR_InputId.WVR_InputId_Alias1_DPad_Up,
	    WVR_InputId.WVR_InputId_Alias1_DPad_Right,
	    WVR_InputId.WVR_InputId_Alias1_DPad_Down,
	    WVR_InputId.WVR_InputId_Alias1_Volume_Up,
	    WVR_InputId.WVR_InputId_Alias1_Volume_Down,
	    WVR_InputId.WVR_InputId_Alias1_Touchpad,
	    WVR_InputId.WVR_InputId_Alias1_Trigger,
	    WVR_InputId.WVR_InputId_Alias1_Digital_Trigger,
	    WVR_InputId.WVR_InputId_Alias1_System
    };

    WVR_InputId[] axisIds = new WVR_InputId[] {
	    WVR_InputId.WVR_InputId_Alias1_Touchpad,
	    WVR_InputId.WVR_InputId_Alias1_Trigger
    };
    

    private void HandleConnectionStatus(bool _value)
    {
        #if UNITY_EDITOR
        Debug.Log (device + " is " + (_value ? "connected" : "disconnected"));
        #endif
        Log.d (LOG_TAG, device + " is " + (_value ? "connected" : "disconnected"));
    }

    
    // press down
    #region Button Press Down
    private void HandlePressDownMenu()
    {
        #if UNITY_EDITOR
        Debug.Log (WVR_InputId.WVR_InputId_Alias1_Menu + " press down");
        #endif

    }


    private void HandlePressDownTouchpad()
    {
        #if UNITY_EDITOR
        Debug.Log (WVR_InputId.WVR_InputId_Alias1_Touchpad + " press down");
        #endif
    }

    private void HandlePressDownTrigger()
    {
        #if UNITY_EDITOR
        Debug.Log (WVR_InputId.WVR_InputId_Alias1_Trigger + " press down");
        #endif
    }
    
    
    private void HandlePressDownDigitalTrigger()
    {
		#if UNITY_EDITOR
	    Debug.Log (WVR_InputId.WVR_InputId_Alias1_Trigger + " press down");
		#endif
        _triggerState = true;
    }
    #endregion

    //press up
    #region Button Press Up
    private void HandlePressUpMenu()
    {
        #if UNITY_EDITOR
        Debug.Log (WVR_InputId.WVR_InputId_Alias1_Menu + " press up");
        #endif
    }

    private void HandlePressUpGrip()
    {
        #if UNITY_EDITOR
        Debug.Log (WVR_InputId.WVR_InputId_Alias1_Grip + " press up");
        #endif
    }

    private void HandlePressUpTouchpad()
    {
        #if UNITY_EDITOR
        Debug.Log (WVR_InputId.WVR_InputId_Alias1_Touchpad + " press up");
        #endif
    }

    private void HandlePressUpTrigger()
    {
        #if UNITY_EDITOR
        Debug.Log (WVR_InputId.WVR_InputId_Alias1_Trigger + " press up");
        #endif
    }


    private void HandlePressUpDigitalTrigger()
    {
		#if UNITY_EDITOR
	    Debug.Log (WVR_InputId.WVR_InputId_Alias1_Trigger + " press up");
		#endif
        _triggerState = false;
    }
    
    #endregion

    
    //touch down
    #region Button Touch Down
    private void HandleTouchDownTouchpad()
    {
        #if UNITY_EDITOR
        Debug.Log (WVR_InputId.WVR_InputId_Alias1_Touchpad + " touch down");
        #endif
        Log.d (LOG_TAG, "button " + WVR_InputId.WVR_InputId_Alias1_Touchpad + " touch down.");
    }

    private void HandleTouchDownTrigger()
    {
        #if UNITY_EDITOR
        Debug.Log (WVR_InputId.WVR_InputId_Alias1_Trigger + " touch down");
        #endif
        Log.d (LOG_TAG, "button " + WVR_InputId.WVR_InputId_Alias1_Trigger + " touch down.");
    }
    #endregion

    //touch up
    #region Button Touch Up
    private void HandleTouchUpTouchpad()
    {
        #if UNITY_EDITOR
        Debug.Log (WVR_InputId.WVR_InputId_Alias1_Touchpad + " touch up");
        #endif
        Log.d (LOG_TAG, "button " + WVR_InputId.WVR_InputId_Alias1_Touchpad + " touch up.");
    }

    private void HandleTouchUpTrigger()
    {
        #if UNITY_EDITOR
        Debug.Log (WVR_InputId.WVR_InputId_Alias1_Trigger + " touch up");
        #endif
        Log.d (LOG_TAG, "button " + WVR_InputId.WVR_InputId_Alias1_Trigger + " touch up.");
    }
    #endregion


    //swipe
    /// Event handling function
    void OnEvent(params object[] args)
    {
	    WVR_Event_t _event = (WVR_Event_t)args[0];
	    // Check the event, e.g. WVR_EventType_RecenterSuccess
	    if (_event.common.type == WVR_EventType.WVR_EventType_RightToLeftSwipe)
	    { 
	    }

	    if (_event.common.type == WVR_EventType.WVR_EventType_LeftToRightSwipe)
	    {
	    } 
    }

    public void OnEnable ()
    {
        var _ctrllsn = WaveVR_ControllerListener.Instance;

        if (_ctrllsn != null)
        {
            if (device != WVR_DeviceType.WVR_DeviceType_HMD)
            {
                #if UNITY_EDITOR
                Debug.Log ("Register " + device + " callback functions.");
                #endif
                Log.d (LOG_TAG, "Register " + device + " callback functions.");
                _ctrllsn.Input (device).ConnectionStatusListeners += HandleConnectionStatus;
                
                // press
                _ctrllsn.Input (device).PressDownListenersMenu += HandlePressDownMenu;
                _ctrllsn.Input (device).PressDownListenersTouchpad += HandlePressDownTouchpad;
                _ctrllsn.Input (device).PressDownListenersTrigger += HandlePressDownTrigger;
                
                _ctrllsn.Input (device).PressUpListenersMenu += HandlePressUpMenu;
                _ctrllsn.Input (device).PressUpListenersTouchpad += HandlePressUpTouchpad;
                _ctrllsn.Input (device).PressUpListenersTrigger += HandlePressUpTrigger;

                _ctrllsn.Input(device).PressDownListenersDigitalTrigger += HandlePressDownDigitalTrigger;
                _ctrllsn.Input(device).PressUpListenersDigitalTrigger += HandlePressUpDigitalTrigger;
                
                //touch
                _ctrllsn.Input (device).TouchDownListenersTouchpad += HandleTouchDownTouchpad;
                _ctrllsn.Input (device).TouchDownListenersTrigger += HandleTouchDownTrigger;
                
                _ctrllsn.Input (device).TouchUpListenersTouchpad += HandleTouchUpTouchpad;
                _ctrllsn.Input (device).TouchUpListenersTrigger += HandleTouchUpTrigger;
                
                //swipe
                WaveVR_Utils.Event.Listen (WaveVR_Utils.Event.ALL_VREVENT, OnEvent);
            }
        }
    }


    public bool IsTriggerPressed()
    {
        return _triggerState;
    }

}




























