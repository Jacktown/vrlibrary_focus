﻿using UnityEngine;
using System.Collections.Generic;



public class TeleportProjector : MonoBehaviour {


    public GameObject anchor;
    public GameObject pointer = null;
    public GameObject beam;

    public float stepDist = 0.50f;
    public float dropDist = 0.20f;
    public float maxDist  = 20.0f;
    public float beamWide = 0.20f;
    public float beamDeep = 0.02f;

    public MeshFilter   meshFilter;
    public MeshRenderer meshRenderer;
    public GameObject   teleportMarker;

    public UserControllerListener scUCL;
    private bool triggerDownVR = false;

    bool pressing = false;
    bool hitting  = false;
    bool isAllowed = false;
    Vector3 hitPoint = Vector3.zero;



    void Update() {
        if (pointer == null)
        {
            pointer = GameObject.Find("ControllerPointerR");
            beam = GameObject.Find("BeamR");
        }
        
        //
        //  First, determine whether the relevant control button is/was down-
        bool oldPress = pressing;
        pressing = false;
        triggerDownVR = scUCL.IsTriggerPressed();
        if (Input.GetKey(KeyCode.P) || triggerDownVR) {
            pressing = true;
        }
        //
        //  If so, construct a beam projecting out from the pointer, position
        //  the teleport marker at the end, and teleport once the button is
        //  released-
        if (pressing || oldPress) {
            this.transform.rotation = Quaternion.identity;
            this.transform.localScale = Vector3.one;

            List<Vector3> beamPoints = UpdateBeamPoints();
            UpdateBeamMesh(beamPoints);

            pointer.SetActive(false);
            beam.SetActive(false);
            meshRenderer.enabled = true;

            Vector3 markerPos = hitPoint;
            markerPos.y += beamDeep;
            teleportMarker.SetActive(hitting);
            teleportMarker.transform.position = markerPos;

            if (! pressing && isAllowed && hitPoint != Vector3.zero) {
                hitPoint.y = anchor.transform.position.y;
                anchor.transform.position = hitPoint;
            }
            else
            {
                //change color
            }
        }
        //
        //  Otherwise, turn the beam and marker off, and the standard pointer
        //  back on-
        else {
            pointer.SetActive(true);
            beam.SetActive(true);
            meshRenderer.enabled = false;
            teleportMarker.SetActive(false);
        }
    }


    List <Vector3> UpdateBeamPoints() {
        //
        //  Start out heading away from the pointer.  (We subtract our own
        //  origin from recorded points to ensure the generated mesh is correct
        //  in relative space.)
        Vector3 origin     = this.transform.position;
        Vector3 direction  = pointer.transform.forward;
        Vector3 projection = pointer.transform.position;
        float   floorHigh  = 0;
        //
        //  We also establish the correct floor height for added safety-
        /*
        RoomController  roomControl  = Globals.RoomControl ();
        LobbyController lobbyControl = Globals.LobbyControl();
        if (roomControl ) floorHigh = roomControl .floorBounds.transform.position.y;
        if (lobbyControl) floorHigh = lobbyControl.floorBounds.transform.position.y;
        */
        //
        //  Add the pointer's origin as the first point, and start looping-
        List<Vector3> allPoints = new List<Vector3>();
        allPoints.Add(projection - origin);
        hitPoint = Vector3.zero;
        hitting = false;
        while (true) {
            //
            //  Extend the beam at regular intervals and check for intersection
            //  with a collider:
            Ray ray = new Ray(projection, direction);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, stepDist)) {
                string hitTag = hit.transform.gameObject.tag;
                isAllowed =  (hitTag.Equals("AllowTeleport") || hitTag.Equals("InteractionZoneVR"));

                hitting  = hit.point.y < floorHigh + stepDist;
                hitPoint = hit.point;
                allPoints.Add(hitPoint - origin);
                break;
            }
            //
            //  If no collision occurs, advance the beam and drop down slightly.
            projection += direction * stepDist;
            allPoints.Add(projection - origin);
            direction.y -= dropDist;
            direction = Vector3.Normalize(direction);
            //
            //  If the beam winds up outside the maximum distance, stop.
            float totalDist = Vector3.Distance(pointer.transform.position, projection);
            if (totalDist >= maxDist) {
                break;
            }
        }
        //
        //  And return the set of all points-
        return allPoints;
    }


    void UpdateBeamMesh(List <Vector3> allPoints) {
        //
        //  First determine what counts as a sideways vector for thickening the
        //  beam-
        Vector3 forward = pointer.transform.forward;
        Vector3 sideOff = Vector3.zero;
        sideOff.x = 0 - forward.z;
        sideOff.z = forward.x;
        sideOff.Normalize();
        sideOff *= beamWide;
        //
        //  Allocate memory for the vertices-
        int numParts = allPoints.Count;
        Vector3[] meshVerts = new Vector3[numParts * 4];
        int[] meshTris = new int[(numParts - 1) * 6];
        //
        //  Create pairs of vertices on each side of the input points, then
        //  stretch triangles between them-
        for (int i = 0, n = 0; i < numParts; i++) {
            meshVerts[n++] = allPoints[i] + sideOff;
            meshVerts[n++] = allPoints[i] - sideOff;
        }
        for (int i = 0, n = 0, p; i < numParts - 1; i++) {
            p = i * 2;
            meshTris[n++] = p + 0;
            meshTris[n++] = p + 1;
            meshTris[n++] = p + 2;
            meshTris[n++] = p + 3;
            meshTris[n++] = p + 2;
            meshTris[n++] = p + 1;
        }
        //
        //  Finally, assign the vertices and refresh auxiliary data-
        Mesh curveMesh = meshFilter.mesh;
        curveMesh.Clear();
        curveMesh.vertices = meshVerts;
        curveMesh.triangles = meshTris;
        curveMesh.RecalculateNormals();
        curveMesh.RecalculateBounds();
    }

}

