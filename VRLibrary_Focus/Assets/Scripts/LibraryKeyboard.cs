﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityIME;

public class LibraryKeyboard : MonoBehaviour
{

    public InputField inputField;
    public char defaultKey;
    public char majKey;
    private LibraryKeyboardManager keyboardManager;

    private void Start()
    {
        keyboardManager = GameObject.Find("Keyboard").GetComponent<LibraryKeyboardManager>();
    }

    public void AddTxt()
    {
        bool shiftState = keyboardManager.IsShiftPressed();
        bool majState = Xor(keyboardManager.IsCapsLocked(), shiftState);
        if (majState)
        {
            if (shiftState)
            {
                keyboardManager.ToggleShift();
            }

            inputField.text += majKey;
        }
        else
        {
            inputField.text += defaultKey;
        }
    }

    private bool Xor(bool arg1, bool arg2)
    {
        return (arg1) ? !arg2 : arg2;
    }
    
}
