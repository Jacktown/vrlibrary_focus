﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class ShelfData : MonoBehaviour
{

    private Transform transform;
    private List<Book.Size> bookDimension;
    private const float bookScale = 0.47f/2;
    private List<String> bookTitles;

    private Canvas labels;
    private Text startLabel;
    private Text endLabel;

    private void Start()
    {
        transform = GetComponent<Transform>();
        bookDimension = new List<Book.Size>();
        bookTitles = new List<String>();

        labels = transform.Find("Labels").GetComponent<Canvas>();
        
        endLabel = transform.Find("Labels").transform.Find("Stop").GetComponent<Text>();
        startLabel = transform.Find("Labels").transform.Find("Start").GetComponent<Text>();

        labels.enabled = false;
    }


    public void PopulateShelf(Article article)
    {
        float widthMax = 1.3f/2;
        float widthMin = 0.7f/2;
        float heightMax = 1.2f/2;
        float heightMin = 0.8f/2;
        float depthMax = 1.2f/2;
        float depthMin = 0.8f/2;
        
        bookDimension.Add(new Book.Size(Random.Range(widthMin, widthMax), Random.Range(heightMin, heightMax), Random.Range(depthMin, depthMax)));
        
        new Book(article, bookDimension[bookDimension.Count-1],  new Library.Position(transform.position.x + PreviousWidth(bookDimension), transform.position.y + 0.2f, transform.position.z-0.1f), this.gameObject);
        bookTitles.Add(article.title);
    }


    public void UpdateLabels()
    {
        labels.enabled = true;

        startLabel.text = bookTitles[0].Substring(0, (bookTitles[0].Length >= 3) ? 3 : bookTitles[0].Length );
        endLabel.text = bookTitles[bookTitles.Count - 1].Substring(0, (bookTitles[bookTitles.Count - 1].Length >= 3) ? 3 : bookTitles[bookTitles.Count - 1].Length);
    }
    
    
    private float PreviousWidth(List<Book.Size> _bookDimension)
    {
        float res = 0.12f;
        for (int i = 0; i < _bookDimension.Count-1; i++)
        {
            res += _bookDimension[i].width * bookScale;
        }
        res += _bookDimension[_bookDimension.Count-1].width * bookScale / 2;
        return res;
    }
}
