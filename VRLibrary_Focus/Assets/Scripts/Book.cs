﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

public class Book {
    
    public static int bookCount = 0;
    public static List<Book> list = new List<Book>();

    private GameObject book;
    //private Label cover;
    private Size size;
    private Library.Position position;

    private String title;
    private String content;
    private List<String> relatedArticles;
    private String rawArticle;

    public struct Size
    {
        public float width;
        public float height;
        public float depth;

        public Size(float x, float y, float z)
        {
            width = x;
            height = y;
            depth = z;
        }
    }

    
    
    public Book(Article article, Size _size, Library.Position _position, GameObject shelf)
    {
        size = _size;
        position = _position;
        bookCount++;
        list.Add(this);


        title = article.title;
        content = article.content;
        relatedArticles = article.relatedArticles;
        rawArticle = article.rawArticle;
        
        //cover = new Label(title);
        
        InstantiateBook(shelf);
        DebugPrint();
    }


    

    private void InstantiateBook(GameObject parent)
    {
        // place the book at the right place
        book = Object.Instantiate(GameObject.Find("BookModel"));
        book.transform.position = new Vector3(position.posX, position.posY, position.posZ);
        book.transform.localScale = new Vector3(size.depth * 0.8f, size.height, size.width)*0.5f;
        book.name = "Book " + title;
        
        // pimp up the model
        //color
        Color newColor = Random.ColorHSV(0f, 1f, 0.5f, 1f, 0.2f, 0.8f);
        book.transform.GetComponent<Renderer>().material.color = newColor;
        float hue, saturation, value;
        Color.RGBToHSV(newColor, out hue, out saturation, out value);
        
        //join
        Text newText = book.GetComponentInChildren<Text>();
        newText.text = title;
        newText.transform.localScale = new Vector3(1, 1, 1);
        if ((value < 0.6f && saturation > 0.6f) || value < 0.5f || ((hue > 0.6f || hue < 0.1f) && saturation > 0.5f))
            newText.color = Color.white;

        
        book.transform.SetParent(parent.transform);
      
        // set data
        BookData bookData = book.GetComponent<BookData>();
        bookData.SetTitle(title);
        bookData.SetContent(content);
        bookData.SetColor(newColor);
        bookData.SetArticles(relatedArticles);
        bookData.SetRaw(rawArticle);
    }

    public GameObject GetGameObject()
    {
        return book;
    }

    private void DebugPrint()
    {
        //Debug.Log("Book: " + label + " position: " + position.posX + ";" + position.posY + ";" + position.posZ + "\n");
        //Debug.Log("Width: " + size.width + " Height: " + size.height + " Depth: " + size.depth);
    }

    public Vector3 GetPosition()
    {
        return new Vector3(position.posX, position.posY, position.posZ);
    }

}
