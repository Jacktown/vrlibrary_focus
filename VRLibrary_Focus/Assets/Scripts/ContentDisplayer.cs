﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.UIElements;
using UnityEngine.UI;

public class ContentDisplayer : MonoBehaviour
{

	public Text display;
	public Animator bookAnimator;

	private String content;
	private List<int> indexSaver;
	private int page;
	
	public void SetContent(String arg)
	{
		content = arg;
		UpdateDisplayedText();
	}

	private void UpdateDisplayedText()
	{
		display.text = content;
		page = 0;
		
		indexSaver = new List<int>();
		indexSaver.Add(0);
	}

	public void NextPage()
	{
		try
		{	//avoid creating blank page at the end of the article if we keep turning
			String temp = content.Substring(display.cachedTextGenerator.characterCountVisible + indexSaver[page], 2); 

			if (!(indexSaver.Count > page + 1)) // if the next page has never been seen
			{
				indexSaver.Add(display.cachedTextGenerator.characterCountVisible +
				               indexSaver[
					               page]); // we save its start index by counting the number of visible characters 
			}

			page++;
			display.text = content.Substring(indexSaver[page]); // then we just display the next part of the string
		}
		catch (Exception e){}
	}
	
	
	public void PreviousPage()
	{									// just go back to the previous saved index and start to display from that point
		if (page > 0)
		{
			page--;												
			display.text = content.Substring(indexSaver[page]);
		}
		else
		{
			bookAnimator.Play("CloseBook");
		}
	}
}
