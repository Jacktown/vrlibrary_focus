﻿public class Tuple<T, U>
{
    public Tuple(){}

    public Tuple(T x, U y)
    {
        this.x = x;
        this.y = y;
    }
		
    public T x { get; set; }
    public U y { get; set; }
		
}
