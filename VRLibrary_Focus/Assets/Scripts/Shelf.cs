﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Timeline;
using UnityEngine.UI;

public class Shelf{

    private GameObject shelf;
    private int shelfID;
    private int nbBooks;
    private Library.Position position;
    
    public static int shelfCount = 0;
    public static List<Shelf> list = new List<Shelf>();

    
    public Shelf(int shelfNumber, int bookQuantity, Library.Position _position, GameObject bookShelf)
    {
        shelfID = shelfNumber;
        nbBooks = bookQuantity;
        position = _position;

        shelf = new GameObject("Shelf " + shelfID);
        shelf.transform.position = new Vector3(position.posX, position.posY, position.posZ);
        shelf.transform.SetParent(bookShelf.transform);
        shelfCount++;
        
        list.Add(this);
        InstantiateShelf(bookShelf);
        
        //DebugPrint();
    }
 


    private void InstantiateShelf(GameObject parent)
    {
        // place the shelf
        GameObject newShelf = Object.Instantiate(GameObject.Find("ShelfModel"));
        newShelf.transform.position = new Vector3(position.posX, position.posY, position.posZ);
        newShelf.name = "Shelf " + shelfID + " Model";
        
        newShelf.transform.SetParent(parent.transform);
    }

    public GameObject GetGameObject()
    {
        return shelf;
    }


    private void DebugPrint()
    {
        Debug.Log("Shelf n#" + shelfID + " position: " + position.posX + ";" + position.posY + ";" + position.posZ + "\n");
    }
}
