﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using HtmlAgilityPack;
using System.Net;
using System.Text.RegularExpressions;


public class WikiScraper : MonoBehaviour
{

    public InputField inputField;
    public Library library;
    
    public Text debugText;
    public GameObject player;
    

    private WWW webpage;
    private HtmlDocument htmlDoc = new HtmlDocument();
    private int bodySize = 40;
    private int limitOfGenerableBooks;

    private String title;
    private String content;

    private Queue<String> urlsToScrap;
    private List<Article> articles;
    private int generatedBook;
    private List<String> scrapedUrls;
        
    public Inventory inventory;


    private void Start()
    {
        urlsToScrap = new Queue<String>();
        articles = new List<Article>();
        scrapedUrls = new List<string>();
        
        limitOfGenerableBooks = library.GetNbGenerableBook();

        //DebugResearch();
    }

    
    public void DebugResearch()
    {
        Initialize();

        debugText.text = "";

        var url = "https://en.wikipedia.org/wiki/Vr";
        webpage = new WWW(url);
        StartCoroutine(OnResponse(true));
    }
    
    
    private void Initialize()         // reset all the lists to new and empty one
    {
        urlsToScrap.Clear();
        articles.Clear();
        scrapedUrls.Clear();
        library.DestroyLibrary();
        library.PopulateLibrary();
        generatedBook = 0;
    }

    
    
    
    public void Research(bool reset)
    {
        if (reset)
        {
            Initialize();
        }

        articles.Clear();
        debugText.text = "";
        
        if (inputField.text != "")
        {
            var url = (inputField.text.StartsWith("https://"))
                ? @"" + inputField.text
                : @"https://en.wikipedia.org/wiki/" + inputField.text.Replace(" ", "_");
            
            
            if(!scrapedUrls.Contains(url))
            {
                scrapedUrls.Add(url);
                webpage = new WWW(url);
                StartCoroutine(OnResponse(true));
            }
        }
    }




    public void ScrapOnSelect(List<String> _articles)
    {
        articles.Clear();
        
        foreach (var article in _articles)
        {
            if (!scrapedUrls.Contains(article))
            {
                urlsToScrap.Enqueue(article);
                scrapedUrls.Add(article);
            }           
        }

        if (urlsToScrap.Count > 0)
        {
            webpage = new WWW(urlsToScrap.Dequeue());            
            StartCoroutine(OnResponse(false));
        }
        else
        {
            inventory.ForceScrapButtonOn();
        }

    }
        
        
        
        

    public bool AutoScrapping()     // return true when we finish to scrap all the book in the queue
    {
        if (generatedBook < limitOfGenerableBooks && urlsToScrap.Count > 0)
        {
            var url = urlsToScrap.Dequeue();

            debugText.text = "load : " + url + "\n";
            
            scrapedUrls.Add(url);
            webpage = new WWW(url);
            StartCoroutine(OnResponse(false));
            return false;
        }

        return true;
    }
    
    
    
    private IEnumerator OnResponse(bool auto)        //Wait for the response of the webpage
    {
        yield return webpage;
        QuickScrap(auto);
    }



    private void QuickScrap(bool auto)
    {
        String link = "";
        List<String> relatedArticlesTemp = new List<String>();

        try
        {
            htmlDoc.LoadHtml(webpage.text);
            var nodeTitle = htmlDoc.DocumentNode.SelectSingleNode("//head/title");
            var nodeLinks =
                htmlDoc.DocumentNode.SelectNodes(
                    "//a[@href and ancestor::div[@id = 'mw-content-text'] and not(ancestor::tbody)]");

            title = nodeTitle.InnerText.Replace(" - Wikipedia", "");

            for (int i = 0; i < nodeLinks.Count; i++)
            {
                if (nodeLinks[i].GetAttributeValue("href", "#error#").StartsWith("/wiki/") &&
                    !Regex.Match(nodeLinks[i].GetAttributeValue("href", "#error#"), @"\.....?$").Success
                )
                {
                    link = WWW.UnEscapeURL("https://en.wikipedia.org" +
                                           nodeLinks[i].GetAttributeValue("href", "#error#"));

                    relatedArticlesTemp.Add(link);
                    
                    if (auto) // add url to scrap automatically if auto scrapping is true
                    {
                        urlsToScrap.Enqueue(link);
                    }
                }
            }

            articles.Add(new Article(title, relatedArticlesTemp, webpage.text));
            generatedBook++;

            if (AutoScrapping())
            {
                EndScrapping();
            }
        }
        catch(Exception e)
        {
            debugText.text = "Sorry... Something went wrong while scrapping:" + title + "\n";
            debugText.text += e.Message;
            if (generatedBook < limitOfGenerableBooks && urlsToScrap.Count > 0)
            {
                if (AutoScrapping())
                {
                    EndScrapping();
                }
            }
            inventory.ForceScrapButtonOn();
        }
    }
    
    
    
    


    public String LayoutScrap(String rawWebsite)            // Scrap the webpage
    {    
        int i = 0;
        String link = "";        
        
        try
        {
            htmlDoc.LoadHtml(rawWebsite);

            var nodeBody = htmlDoc.DocumentNode.SelectNodes(
                "(//p|//h2|//h3|//h4|//ul|//ol)[ancestor::div[@id = 'mw-content-text'] and not(ancestor::tbody)]");

            var nodeLinks =
                htmlDoc.DocumentNode.SelectNodes(
                    "//a[@href and ancestor::div[@id = 'mw-content-text'] and not(ancestor::tbody)]");


            content = "";

            // ----------------------------------------------------- Main Article : create the content
            for (i = 0; i < Math.Min(bodySize, nodeBody.Count); i++)
            {
                if (nodeBody[i].ParentNode.Name == "div" && Regex.Match(nodeBody[i].InnerText, @"[a-z]+").Success)
                {
                    BodyPrint(nodeBody[i]);
                }
                else
                {
                    bodySize++;
                }
            }


            // ---------------------------------------------------- Check the links and add them
            content += " * * * * * * * * * * * List of related articles * * * * * * * * * * *\r\n\r\n";

            for (i = 0; i < nodeLinks.Count; i++)
            {
                if (nodeLinks[i].GetAttributeValue("href", "#error#").StartsWith("/wiki/") &&
                    !Regex.Match(nodeLinks[i].GetAttributeValue("href", "#error#"), @"\.....?$").Success
                ) // No external links or references to the same page and no links finishing with a format type (.jpg, .svg, .ogg ...)
                {
                    String temp = WWW.UnEscapeURL("&#8226; " + nodeLinks[i].GetAttributeValue("title", "#error#")) +
                                  ":\r\nhttps://en.wikipedia.org" + nodeLinks[i].GetAttributeValue("href", "#error#") +
                                  "\r\n";
                    content += temp;
                }
            }
        }
        catch (Exception e)
        {
            content = "Sorry... Something went wrong while scrapping:" + title + "\n";
            content += e.Message;

        }

        return content;
    }

    private void EndScrapping()
    {
        inventory.ForceScrapButtonOn();
        
        debugText.text = "Book generated : " + articles.Count + "\n";

        articles.Sort((a1,a2) => a1.title.CompareTo(a2.title)); 
        library.AddBooks(articles);
    }
    
       
    
    private void BodyPrint(HtmlNode nodeBody)
    {
        try
        {
            switch (nodeBody.Name)
            {
                case "p":                                                                                                   // Paragraphs
                    content += WWW.UnEscapeURL(nodeBody.InnerText) + "\r\n\r\n";
                    break;
                case "h2":                                                                                                  // Titles with different indentation
                    content += "* * * * * * * * * * * " + WWW.UnEscapeURL(nodeBody.InnerText) + " * * * * * * * * * * *\r\n\r\n";
                    break;
                case "h3":
                    content += "----- " + WWW.UnEscapeURL(nodeBody.InnerText) + " -----\r\n\r\n";
                    break;
                case "h4":
                    content += "   " + WWW.UnEscapeURL(nodeBody.InnerText) + "\r\n\r\n";
                    break;
                case "ul":                                                                                                  // Unordered list
                    RecursiveSearch(nodeBody);
                    content +="\r\n";
                    break;
                case "ol":                                                                                                  // Ordered list
                    int i = 1;
                    foreach (HtmlNode child in nodeBody.ChildNodes)
                        if (child.Name == "li")
                        {
                            content += i + " " + WWW.UnEscapeURL(child.InnerText) + "\r\n"; 
                            i++;
                        }

                    content += "\r\n";
                    break;
            }
        }
        catch (NullReferenceException)
        {
            content +=  WWW.UnEscapeURL(nodeBody.InnerText) + " #Format Error#\r\n\r\n";
        }
    }
    
    private void RecursiveSearch(HtmlNode ul)
    {
        foreach (HtmlNode node in ul.SelectNodes("./li"))
        {
            if (node.Element("ul") != null)
            {
                PrintListElmt(node.Element("a").InnerText);
                RecursiveSearch(node.Element("ul"));
            }
            else
            {
                PrintListElmt(node.InnerText);
            }
        }
    }

    private void PrintListElmt(string element)
    {
        int nbMatch = Regex.Matches(element, "^[0-9]+\\.").Count;
        if (nbMatch > 0)
        {
            content += new String(' ', 5 * nbMatch) + WWW.UnEscapeURL(element) + "\r\n";
        }
        else
        {
            content += WWW.UnEscapeURL("&#8226; " + element) + "\r\n";
        }
            
    }
}


public class Article
{
    public readonly String title;
    public readonly String content = "";
    public List<String> relatedArticles;
    public readonly String rawArticle;
    
    public Article(String title, String content, List<String> articles)
    {
        this.title = title;
        this.content = content;
        this.relatedArticles = articles;
    }
    
    public Article(String title, List<String> articles, String rawArticle)
    {
        this.title = title;
        this.relatedArticles = articles;
        this.rawArticle = rawArticle;
    }


}
