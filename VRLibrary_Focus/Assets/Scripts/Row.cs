﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Row {

    private GameObject row;
    private int rowID;
    private int nbBookShelves;
    private Library.Position position;
    private Library librarySettings;
    private const float shelfWidth = 7f/4;

    public static int rowCount = 0;
   
  
    public Row(int rowNumber, int bookShelvesQuantity, Library.Position _position, GameObject library)
    {
        rowID = rowNumber;
        nbBookShelves = bookShelvesQuantity;
        position = _position;

        row = new GameObject("Row " + rowID);
        row.transform.position = new Vector3(position.posX, position.posY, position.posZ);
        row.transform.SetParent(library.transform);
        
        rowCount++;
        librarySettings = GameObject.Find("LibraryManager").GetComponent<Library>();
        PopulateRow();
        
        //DebugPrint();
    }
    
    
    private void PopulateRow()
    {
        for (int i = 0; i < nbBookShelves; i++)
        {
            new BookShelf(i + rowID * nbBookShelves, librarySettings.bookShelfHeight, 
                new Library.Position(position.posX + i * shelfWidth, 0, position.posZ), row);
        }
    }
    

    private void DebugPrint()
    {
        Debug.Log("Row n#" + rowID + " position: " + position.posX + ";" + position.posY + ";" + position.posZ + "\n");
    }
}

