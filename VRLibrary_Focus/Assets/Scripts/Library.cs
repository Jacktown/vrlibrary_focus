﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Library : MonoBehaviour
{

    [Range(1, 50)]
    public int nbColumn = 3;
    [Range(1, 50)]
    public int nbRow = 4;
    [Range(1, 50)]
    public int sizeRow = 2;
    [Range(2, 8)]
    public int bookShelfHeight = 5;
    [Range(1, 14)]
    public int booksPerShelf = 12;
    [Range(0.01f, 1f)]
    public float clippingPeriod = 0.1f;
    public float radiusFOV = 5;


    public GameObject headset;
    public GameObject FOV;

    private GameObject library;
    private const int shelfWidth = 4;
    private const int columnSpace = 10/4;
    private float lastTime;
    public const int rowSpace = 12/4;


    private int shelfToFill;
    private GameObject[] shelves;

    public struct Position
    {
        public float posX;
        public float posY;
        public float posZ;

        public Position(float x, float y, float z)
        {
            posX = x;
            posY = y;
            posZ = z;
        }
    }

    void Start()
    {
        Physics.gravity = new Vector3(0, -60.0F, 0);
        headset.transform.position = new Vector3(12,0, -9);
        PopulateLibrary();
        lastTime = 0;
        //Debug.Log("Row: " + Row.rowCount + "; BookShelf: " + BookShelf.bookShelfCount + "; Shelf: " + Shelf.shelfCount + "; Book: " + Book.bookCount);
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(1))
        {
            DestroyLibrary();
            //Debug.Log("Row: " + Row.rowCount + "; BookShelf: " + BookShelf.bookShelfCount + "; Shelf: " + Shelf.shelfCount + "; Book: " + Book.bookCount);
        }
        if (Time.fixedTime - lastTime > clippingPeriod)
        {
            BookClipping();
            lastTime = Time.fixedTime;
        }
        Destroy(GameObject.Find("Cube")); 
    }



    public void PopulateLibrary()
    {
        headset.transform.position = new Vector3(12,0, -9);
        shelfToFill = 1; // index to know which shelve fill. don't start at 0 because it's the model
        
        library = GameObject.CreatePrimitive(PrimitiveType.Cube); // Instantiate floor
        library.transform.position = new Vector3((sizeRow * shelfWidth * nbColumn + (nbColumn - 1) * columnSpace) / 2, -0.25f / 2, nbRow * rowSpace / 2 - 2.5f);
        library.transform.localScale = new Vector3(sizeRow * shelfWidth * nbColumn + nbColumn * columnSpace + 10, 0.1f, nbRow * rowSpace);
        library.name = "Library";
        
        
        GameObject floor = Instantiate(GameObject.CreatePrimitive(PrimitiveType.Cube));
        floor.transform.position = new Vector3((sizeRow * shelfWidth * nbColumn + (nbColumn - 1) * columnSpace) / 2, -0.25f / 2, nbRow * rowSpace / 2 -2.5f);
        floor.transform.localScale = new Vector3(sizeRow * shelfWidth * nbColumn + nbColumn * columnSpace + 10, 0.26f, nbRow * rowSpace);
        floor.name = "Floor";
        floor.tag = "AllowTeleport";        
        floor.transform.parent = library.transform;

        for (int i = 0; i < nbColumn; i++)
        {
            new Column(i, nbRow, new Position(i * (sizeRow * shelfWidth + columnSpace), 0, 0), library);
        }       
    }



    public void AddBooks(List<Article> articles)
    {
        shelves = GameObject.FindGameObjectsWithTag("Shelf");
        
        int i = 0;
        while (i < articles.Count && shelfToFill < shelves.Length)
        {
            ShelfData shelf = shelves[shelfToFill].GetComponent<ShelfData>();
            
            for (int j = 0; j < booksPerShelf; j++)
            {
                if (i < articles.Count)
                {
                    shelf.PopulateShelf(articles[i]); 
                    i++;
                }
            }

            shelf.UpdateLabels();
            shelfToFill++;
        }

        if (articles.Count > 0)
        {
            shelfToFill = ((shelfToFill - 1) % 5 == 0) ? shelfToFill:(((shelfToFill-1) / bookShelfHeight)+1)*bookShelfHeight+1;
        }
        
    }
    
    
    
    public void DestroyLibrary()
    {
        Destroy(library);
        Row.rowCount = 0;
        BookShelf.bookShelfCount = 0;
        Shelf.shelfCount = 0;
        Book.bookCount = 0;
        
        BookShelf.list.Clear();
        Shelf.list.Clear();
        Book.list.Clear();

        shelfToFill = 1;
    }

    private void BookClipping()
    {
        Collider[] hitColliders = Physics.OverlapSphere(FOV.transform.position, radiusFOV);
        
        for (int i = 0; i < Book.bookCount; i++)
        {
            GameObject book = Book.list[i].GetGameObject();
            Collider bookCollider = book.GetComponent<Collider>();
            Canvas bookCanvas = book.GetComponentInChildren<Canvas>();

            if (hitColliders.Contains(bookCollider))
            {
                if (!bookCanvas.enabled)
                {
                    bookCanvas.enabled = true;
                }
            }
            else
            {
                if (bookCanvas.enabled)
                {
                    bookCanvas.enabled = false;
                }
            }
        }
    }


    public Vector3 PositionToVector3(Position pos)
    {
        return new Vector3(pos.posX, pos.posY, pos.posZ);
    }

    public int GetNbGenerableBook()
    {
        return booksPerShelf * bookShelfHeight * sizeRow * nbRow * nbColumn;
    }
}


