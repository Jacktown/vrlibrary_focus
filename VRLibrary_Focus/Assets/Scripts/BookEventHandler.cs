﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.Internal.Experimental.UIElements;
using UnityEngine.UI;
using WaveVR_Log;

public class BookEventHandler: MonoBehaviour,
    IPointerHoverHandler

{
    private const string LOG_TAG = "WaveVR_EventHandler";
    private WaitForEndOfFrame waitForEndOfFrame = new WaitForEndOfFrame();
    
    private BookData book;

    public Text titlePreview;
    public GameObject panel;
    public Inventory inventory;
    public UserControllerListener uCL;
    
    private void Start()
    {
        book = GetComponent<BookData>();
        
    }


    #region override event handling function



    public void OnPointerHover (PointerEventData eventData)
    {
        titlePreview.text = book.GetTitle();
        Color temp = book.GetColor();
        panel.GetComponent<Image>().color = new Color(temp.r, temp.g, temp.b, 0.5f);
        

        if (uCL.IsTouchpadPressed())
        {
            inventory.AddToInventory(book);
            inventory.SaveGameObject(book.GetTitle(), gameObject);

            uCL.ForceTouchpadOff();
        }
    }
   
    #endregion
}