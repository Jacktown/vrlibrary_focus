﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BookData : MonoBehaviour
{

	private String title;
	private String content;
	private Color color;
	private List<String> linkedArticles;
	private String raw;

	private GameObject bookModel;

	public BookData(String title, String content, Color color)
	{
		this.title = title;
		this.content = content;
		this.color = color;
	}

	public BookData(String title, String content)
	{
		this.title = title;
		this.content = content;
	}
	
	
	
	// Use this for initialization
	void Start ()
	{
		bookModel = this.gameObject;
	}

	public void SetTitle(String s)
	{
		title = s;
	}

	public void SetContent(String s)
	{
		content = s;
	}

	public void SetColor(Color c)
	{
		color = c;
	}

	public void SetRaw(String s)
	{
		raw = s;
	}


	public String GetTitle()
	{
		return title;
	}

	public String GetContent()
	{
		return content;
	}

	public GameObject GetGameObject()
	{
		return bookModel;
	}

	public Color GetColor()
	{
		return color;
	}

	public void SetArticles(List<String> links)
	{
		linkedArticles = links;
	}

	public List<String> GetArticles()
	{
		return linkedArticles;
	}

	public String GetRaw()
	{
		return raw;
	}
}
