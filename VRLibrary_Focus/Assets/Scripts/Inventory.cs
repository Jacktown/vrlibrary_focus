﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Inventory : MonoBehaviour
{
	// menu  (general)
	private int width;
	private int height;
	private int itemsPerRows = 11;
	
	public Button scrapButton;
	public Button selectButton;
	
	
	//items
	private List<Tuple<GameObject, BookData>> listItems;
	private int selectIndex;
	private GameObject activeSelector;
	private SortedList<String, GameObject> booksDeactivated;
	
	public GameObject modelItem;

	
	//readable book
	public ContentDisplayer readableBookTitle;
	public ContentDisplayer readableBookContent;

	//scraper
	public WikiScraper wikiScraper;
	
	
	void Start () {
		listItems = new List<Tuple<GameObject, BookData>>();
		booksDeactivated = new SortedList<string, GameObject>();
		selectButton.interactable = false;
		scrapButton.interactable = false;
	}
	
	
	private bool IsAlreadyPresent(BookData book)
	{
		// check if a book is present in the inventory
		int i = 0;
		bool found = false;
		while (i < listItems.Count && !found)
		{
			found = listItems[i].y.Equals(book);
			i++;
		}

		return found;
	}
	
	
	
	private void UpdateInventory()
	{
		// Dispose the items on the menu according to the number of items
		
		int x, y; // Coordinate to place the items
		int nbItems = listItems.Count;
		int nbRows = (int) Math.Floor( (nbItems-1) / itemsPerRows + 1.0f);
		int nbItemsLastRows = nbItems % itemsPerRows;
		
		// How much space needed between each items
		int horizontalPos, verticalPos;
		// (0,0) = center of the canvas. I place the items like if (0,0) was the bottom left corner to not deal with negative coordinates
		// then move the items with the offset, so they're centred
		int horizontalOffset, verticalOffset;
		int itemsPlaced = 0;
		
		//dimensions of the canvas
		width = (int) gameObject.GetComponent<RectTransform>().sizeDelta.x;
		height = (int) gameObject.GetComponent<RectTransform>().sizeDelta.y;
		
		verticalPos = height / (nbRows + 1);
		horizontalOffset = width / 2;
		verticalOffset = height / 2;
		
		for (y = verticalPos; y <= nbRows*verticalPos; y+= verticalPos)
		{
			horizontalPos = (itemsPlaced > nbItems - itemsPerRows)? width / (nbItemsLastRows + 1):width / (itemsPerRows + 1);
			int nbItemsThisRow = (itemsPlaced > nbItems - itemsPerRows) ? nbItemsLastRows : itemsPerRows;
			
			for (x = horizontalPos; x <= nbItemsThisRow*horizontalPos; x += horizontalPos)
			{
				RectTransform rectTransform = listItems[itemsPlaced].x.GetComponent<RectTransform>();
				rectTransform.localPosition = new Vector3(x-horizontalOffset, y-verticalOffset);
				rectTransform.localEulerAngles = new Vector3(0,0,0);
				
				itemsPlaced++;
			}
		}		
	}
	
	
	
	public void AddToInventory(BookData book)
	{
		if (!IsAlreadyPresent(book))
		{
			GameObject item = GameObject.Instantiate(modelItem, gameObject.transform, true);
			
			// item color
			Image itemImage = item.transform.Find("Panel").GetComponent<Image>();
			Color tempColor = book.GetColor();
			itemImage.color = new Color(tempColor.r, tempColor.g, tempColor.b, 0.5f);
			
			// item text
			Text itemTitle = item.transform.Find("Text").GetComponent<Text>();
			itemTitle.text = book.GetTitle();

			GameObject selector = item.transform.Find("Selector").gameObject;
			if (listItems.Count >= 1)
			{				
				selector.SetActive(false);
			}
			else // first book added : it's automatically the item pre-selected + activate interaction buttons
			{
				activeSelector = selector;
				selectIndex = 0;
				
				selectButton.interactable = true;
				scrapButton.interactable = true;
			}
		
			listItems.Add(new Tuple<GameObject, BookData>(item, book));
			
			UpdateInventory();
		}
	}


	public void SaveGameObject(String title, GameObject bookModel)
	{
		booksDeactivated.Add(title, bookModel);
		bookModel.SetActive(false);
	}

	private void ReplaceInLibrary()
	{
		String title = listItems[selectIndex].y.GetTitle();
		try
		{
			booksDeactivated[title].SetActive(true);
		}
		catch
		{
		}
		finally
		{
			booksDeactivated.RemoveAt(booksDeactivated.IndexOfKey(title));
		}
	}
	

	public void RemoveItem()
	{
		if (listItems.Count > 0)
		{
			ReplaceInLibrary();
			
			listItems[selectIndex].x.gameObject.SetActive(false);
			listItems.RemoveAt(selectIndex);
			if (listItems.Count > 0)
			{
				if (selectIndex >= listItems.Count)
				{
					selectIndex--;
				}
				
				activeSelector = listItems[selectIndex].x.transform.Find("Selector").gameObject;
				activeSelector.SetActive(true);			
			}
			else
			{
				selectButton.interactable = false;
				scrapButton.interactable = false;
			}
			
			UpdateInventory();
		}
	}

	
	public void IncSelector()
	{
		if (listItems.Count > 0)
		{
			if (selectIndex < listItems.Count-1)
			{
				activeSelector.SetActive(false);
				selectIndex++;
				activeSelector = listItems[selectIndex].x.transform.Find("Selector").gameObject;
				activeSelector.SetActive(true);
			}			
		}
	}
	
	public void DecSelector()
	{
		if (listItems.Count > 0)
		{
			if (selectIndex > 0)
			{
				activeSelector.SetActive(false);
				selectIndex--;
				activeSelector = listItems[selectIndex].x.transform.Find("Selector").gameObject;
				activeSelector.SetActive(true);
			}			
		}
	}


	public void SelectBook()
	{
		if (listItems.Count > 0)
		{
			if (listItems[selectIndex].y.GetContent() == "")
			{
				listItems[selectIndex].y.SetContent(wikiScraper.LayoutScrap(listItems[selectIndex].y.GetRaw()));
			}
			
			readableBookTitle.SetContent(listItems[selectIndex].y.GetTitle());
			readableBookContent.SetContent(listItems[selectIndex].y.GetContent());
		}
	}


	public void ScrapRelated()
	{
		if (listItems.Count > 0)
		{
			ToggleScrap();
			wikiScraper.ScrapOnSelect(listItems[selectIndex].y.GetArticles());
		}	
	}

	public void ToggleScrap()
	{
		scrapButton.interactable = !scrapButton.interactable;
	}

	public void ForceScrapButtonOn()
	{
		scrapButton.interactable = true;
	}
}
